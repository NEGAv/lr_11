﻿#include <iostream>
#include <string> 
#include "windows.h"
using namespace std;
int fcountA(char S[], int countA ) {

    for (int i = 0; S[i] < 101; i++) 
    {
        if ((S[i] == 'а') || (S[i] == 'А'))
            countA++;
    }
    return countA;
}
int fcountB(char S[], int countB) {

    for (int i = 0; S[i] < 101; i++)  
    {
         if ((S[i] == 'б') || (S[i] == 'Б'))
            countB++;
    }
    return countB;
}
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int n = 101;
    int countA = 0, countB = 0;
    char S[n];
    cout << "Введіть рядок не більше 100 символів\n";
    gets_s(S);  // майже те саме , що і cin 
    cout << '\t' << "Ви ввели рядок: " << endl << S << endl;
    int A = fcountA(S, countA);
    int B = fcountB(S, countB);
    cout << "Кількість літер А в рядку: " << A << endl << "Кількість літер Б в рядку: " << B << endl;
    cout << "Кількість літер А більша за кількість Б ?" << endl;
    cout << boolalpha << '\t' << (A > B) << endl;
}